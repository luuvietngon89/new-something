import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { User } from './user.model';
import { Jsonp } from '@angular/http/src/http'

@Injectable()
export class LoginService {

  constructor(private Http: Http) { }

  postLogin(emp : User){
    var body = JSON.stringify(emp);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var RqOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    console.log(this.Http.get('http://localhost:47493/api/users/login').map((data : Response)=>{
      return data.json()as User[];
    }));
    return this.Http.post('http://localhost:47493/api/users/login',body,RqOptions).map(x => x.json());
  }

}
