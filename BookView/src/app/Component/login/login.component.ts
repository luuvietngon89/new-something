import { Component, OnInit } from '@angular/core';
import { LoginService } from './shared/login.service';
import { NgForm } from '@angular/forms';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[LoginService]
})

export class LoginComponent implements OnInit {

  constructor(private LoginService :LoginService,private toastr : ToastrService) { }

  ngOnInit() {
  }

  onSubmit(loginForm:NgForm) {
    //console.log(loginForm.value);
    //loginForm.preventDefault();
    //var username= loginForm.target.element[0].value;
    //var password = loginForm.target.element[1].value;
    this.LoginService.postLogin(loginForm.value).subscribe(x => {
      console.log(x);
    });
    console.log(loginForm.value);
    }
}