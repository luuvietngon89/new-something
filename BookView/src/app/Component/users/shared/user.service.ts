import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { User } from './user.model';
import { Jsonp } from '@angular/http/src/http'

@Injectable()
export class UserService {

  selectedUser : User;
  UserList : User[];
  constructor(private Http: Http) { }

  postUser(emp : User){
    var body = JSON.stringify(emp);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var RqOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.Http.post('http://localhost:47493/api/Users',body,RqOptions).map(x => x.json());
  }
  getUserList(){
     this.Http.get('http://localhost:47493/api/Users')
     .map((data : Response)=>{
       return data.json()as User[];
     }).toPromise().then(x=>{this.UserList=x;
     })
    //return this.Http.get('http://localhost:47493/api/Users');
  }
}
