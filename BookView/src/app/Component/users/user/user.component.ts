import { Component,OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../shared/user.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers:[UserService]
})
export class UserComponent implements OnInit {

  constructor(private UserService :UserService, private toastr : ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form? : NgForm){
    if(form!=null)
      form.reset();
    this.UserService.selectedUser = {
      UserName :'',
      Password :'',
      Email :'',
      IsActive:false
    }
  }
  onSubmit(form : NgForm){
    this.UserService.postUser(form.value).subscribe( data =>{
      this.resetForm(form);
      this.toastr.success('New Record Added Successfully','User Register');
    })
  }
}
