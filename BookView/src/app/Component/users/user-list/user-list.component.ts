import { Component, OnInit } from '@angular/core';

import { UserService } from '../shared/user.service';
import { User } from '../shared/user.model';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  providers:[UserService]
})
export class UserListComponent implements OnInit {

  constructor(private UserService : UserService) { }

  ngOnInit() {
    console.log(this.UserService.getUserList());
    this.UserService.getUserList();
  }
  showForEdit(emp : User)
  {
    this.UserService.selectedUser = Object.assign({},emp);;
  }
}
