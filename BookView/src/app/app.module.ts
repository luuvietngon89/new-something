import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { UserComponent } from './component/users/user/user.component';
import { UsersComponent } from './component/users/users.component';
import { UserListComponent } from './component/users/user-list/user-list.component';
import { UserService } from './component/users/shared/user.service';
import { LoginComponent } from './component/login/login.component';
import { HomeComponent } from './Component/home/home.component';
import { BookManagementComponent } from './Component/book-management/book-management.component';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UsersComponent,
    UserListComponent,
    LoginComponent,
    HomeComponent,
    BookManagementComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
